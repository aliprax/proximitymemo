/**
 * 
 */
package com.example.proximitymemo;

import it.polimi.dei.spf.framework.local.SPFActionIntent;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

/**
 * @author Jacopo
 *
 */
public class TriggerActionReceiver extends BroadcastReceiver {

	/**
	 * 
	 */
	public TriggerActionReceiver() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context arg0, Intent arg1) {
		if (arg1.getAction().equals(arg0.getPackageName()+".SPFAction")){
			reactToAction(arg0,arg1);
		}

	}

	private void reactToAction(Context arg0, Intent arg1) {
		String trgName = arg1.getStringExtra(SPFActionIntent.ARG_STRING_TRIGGER_NAME);
		String targetId = arg1.getStringExtra(SPFActionIntent.ARG_STRING_TARGET);
		String displayname = arg1.getStringExtra(SPFActionIntent.ARG_STRING_DISPLAY_NAME);		
		String contentText= trgName+" has found "+selectEntityName(targetId,displayname);
				
		Notification.Builder builder = new Notification.Builder(arg0);
		Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Notification n = builder
			.setAutoCancel(true)
			.setContentTitle("Memo")
			.setContentText(contentText)
			.setSmallIcon(R.drawable.ic_launcher)
			.setSound(alarmSound)
			.build();
		NotificationManager nm = (NotificationManager) arg0.getSystemService(Context.NOTIFICATION_SERVICE);
		int id=1;
		nm.notify(id, n);
		
	}

	private String selectEntityName(String targetId, String displayname) {
		if(displayname!=null&&!displayname.trim().equals("")){
			return displayname;
		}else if (targetId!=null&&!targetId.trim().equals("")){
			return targetId;
		}else{
			return " an anonimous entity";
		}
	}

}
