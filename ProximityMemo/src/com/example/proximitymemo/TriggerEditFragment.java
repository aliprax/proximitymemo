/**
 * 
 */
package com.example.proximitymemo;

import java.util.LinkedList;
import java.util.List;

import it.polimi.dei.spf.framework.local.Query;
import it.polimi.dei.spf.framework.local.SPFAction;
import it.polimi.dei.spf.framework.local.SPFActionIntent;
import it.polimi.dei.spf.framework.local.SPFActionSendNotification;
import it.polimi.dei.spf.framework.local.SPFTrigger;

import com.example.proximitymemo.view.TagsPicker;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;

/**
 * @author Jacopo
 * 
 */
public class TriggerEditFragment extends Fragment {

	/*
	 * see strings.xml : @array/trigger_action_entries order
	 */
	private static final int SPF_ACTION_INTENT_ID = 0;
	private static final int SPF_ACTION_MESSAGE_ID = 1;

	TagsPicker tagsPicker;
	EditText editName;
	EditText editSleep;
	CheckBox oneshotCheck;
	Spinner actionSpinner;
	EditText editMessageTitle;
	EditText editMessageContent;

	/**
	 * 
	 */
	public TriggerEditFragment() {

	}

	public static TriggerEditFragment newInstance(Bundle args) {
		TriggerEditFragment tef = new TriggerEditFragment();
		tef.setArguments(args);
		return tef;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_trigger_edit,
				container, false);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		FrameLayout tagsPickerplaceholder = (FrameLayout) getView()
				.findViewById(R.id.tags_picker_placeholder);
		tagsPicker = new TagsPicker(getActivity());
		tagsPickerplaceholder.addView(tagsPicker);
		editName = (EditText) view.findViewById(R.id.edit_trigger_name);
		editSleep = (EditText) view.findViewById(R.id.sleep_time_edit);
		actionSpinner = (Spinner) view
				.findViewById(R.id.trigger_action_spinner);
		editMessageContent = (EditText) view
				.findViewById(R.id.action_message_content_edit);
		editMessageTitle = (EditText) view
				.findViewById(R.id.action_message_title_edit);
		oneshotCheck = (CheckBox) view.findViewById(R.id.oneshot_checkbox);
		SPFTrigger trigger = ((Callbacks) getActivity()).getTrigger();
		List<String> initialTags;
		int actionId = SPF_ACTION_INTENT_ID;
		String initialName = null, messageTitle = null, messageContent = null;
		boolean isOneShot;
		long sleeptime;
		if (trigger != null) {
			Query q = trigger.getQuery();
			initialTags = q.getTags();
			initialName = trigger.getName();
			isOneShot = trigger.isOneShot();
			sleeptime = trigger.getSleepPeriod();
			if (trigger.getAction() instanceof SPFActionSendNotification) {
				SPFActionSendNotification action = (SPFActionSendNotification) trigger
						.getAction();
				messageContent = action.getMessage();
				messageTitle = action.getTitle();
				actionId = SPF_ACTION_MESSAGE_ID;
			}

		} else {
			// there is no trigger: edit new
			initialTags = new LinkedList<String>();
			isOneShot = false;
			sleeptime = 10 * 1000;// ten seconds millis
		}
		tagsPicker.setInitialTags(initialTags);
	
		
		
		if (initialName != null) {
			editName.getText().clear();
			editName.getText().append(initialName);
		}
		oneshotCheck.setChecked(isOneShot);
		editSleep.setText(Long.toString(sleeptime / 1000)); // visualized as
															// seconds
		if (oneshotCheck.isChecked()) {
			editSleep.setEnabled(false);
		}
		
		if (messageContent != null) {
			editMessageContent.getText().clear();
			editMessageContent.getText().append(messageContent);
		}
		if (messageTitle != null) {
			editMessageTitle.getText().clear();
			editMessageTitle.getText().append(messageTitle);
		}
		
		updateActionSection(actionId);
		
		addListeners();

	}

	private void addListeners() {
		oneshotCheck
				.setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						editSleep.setEnabled(!isChecked);

					}
				});
		
		actionSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				updateActionSection(position);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	void updateActionSection(int actionId) {
		if (actionId == SPF_ACTION_INTENT_ID) {
			actionSpinner.setSelection(SPF_ACTION_INTENT_ID);
			editMessageContent.setVisibility(View.INVISIBLE);
			editMessageTitle.setVisibility(View.INVISIBLE);
		} else {
			actionSpinner.setSelection(SPF_ACTION_MESSAGE_ID);
			editMessageContent.setVisibility(View.VISIBLE);
			editMessageTitle.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		tagsPicker = null;
		editName = null;
		editSleep = null;
		oneshotCheck = null;
		editMessageContent = null;
		actionSpinner = null;
		editMessageTitle = null;
	}

	@Override
	public void onAttach(Activity activity) {

		super.onAttach(activity);
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"Activity must implement fragment's callbacks");
		}
	}

	public interface Callbacks {
		SPFTrigger getTrigger();
	}

	public List<String> getTags() {
		return tagsPicker.getTags();
	}

	public String getTriggerName() {
		return ((EditText) getView().findViewById(R.id.edit_trigger_name))
				.getText().toString();

	}

	public boolean getIsOneShot() {
		return oneshotCheck.isChecked();
	}

	/**
	 * Return the sleep time in millis
	 * 
	 * @return
	 */
	public long getSleepTime() {
		return Long.parseLong(editSleep.getText().toString()) * 1000;
	}
	
	public SPFAction getSPFAction(){
		int actionId = actionSpinner.getSelectedItemPosition();
		switch(actionId){
		case SPF_ACTION_INTENT_ID:
			SPFActionIntent actI = new SPFActionIntent(getActivity().getPackageName()+".SPFAction");
			return actI;
			
		case SPF_ACTION_MESSAGE_ID:
			String title = editMessageTitle.getText().toString();
			String message = editMessageContent.getText().toString();
			SPFActionSendNotification actSN = new SPFActionSendNotification(title, message);
			return actSN;
			
		default:
			throw new IllegalStateException("action id cannot be resolved");
		}
		
		
	}

}
