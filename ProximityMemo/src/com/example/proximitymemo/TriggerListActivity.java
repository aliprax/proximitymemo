package com.example.proximitymemo;

import it.polimi.dei.spf.framework.local.Permission;
import it.polimi.dei.spf.framework.local.SPFTrigger;
import it.polimi.dei.spf.lib.async.PermissionManager;
import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;




/**
 * An activity representing a list of Triggers. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link TriggerDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 * <p>
 * The activity makes heavy use of fragments. The list of items is a
 * {@link TriggerListFragment} and the item details
 * (if present) is a {@link TriggerDetailFragment}.
 * <p>
 * This activity also implements the required
 * {@link TriggerListFragment.Callbacks} interface
 * to listen for item selections.
 */
public class TriggerListActivity extends Activity
        implements TriggerListFragment.Callbacks, TriggerDetailFragment.Callbacks {

    private static final String TAG_FRAG_DETAIL = "fragment_detail";
	/**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trigger_list);

        if (findViewById(R.id.trigger_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;

            // In two-pane mode, list items should be given the
            // 'activated' state when touched.
            ((TriggerListFragment) getFragmentManager()
                    .findFragmentById(R.id.trigger_list))
                    .setActivateOnItemClick(true);
        }
        //require SPF permission
        PermissionManager.get().requirePermission(Permission.NOTIFICATION_SERVICES);
        // TODO: If exposing deep links into your app, handle intents here.
    }

    /**
     * Callback method from {@link TriggerListFragment.Callbacks}
     * indicating that the item with the given ID was selected.
     */
    @Override
    public void onItemSelected(SPFTrigger trigger) {
        if (mTwoPane) {
            // In two-pane mode, show the detail view in this activity by
            // adding or replacing the detail fragment using a
            // fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putLong(TriggerDetailFragment.ARG_TRIGGER_ID, trigger.getId());
            TriggerDetailFragment fragment = new TriggerDetailFragment();
            fragment.setArguments(arguments);
            getFragmentManager().beginTransaction()
                    .replace(R.id.trigger_detail_container, fragment,TAG_FRAG_DETAIL)
                    .commit();

        } else {
            // In single-pane mode, simply start the detail activity
            // for the selected item ID.
            Intent detailIntent = new Intent(this, TriggerDetailActivity.class);
            detailIntent.putExtra(TriggerDetailFragment.ARG_TRIGGER_ID, trigger.getId());
            startActivity(detailIntent);
        }
    }

	@Override
	public void onItemDeleted() {
		TriggerListFragment fr =(TriggerListFragment) getFragmentManager().findFragmentById(R.id.trigger_list);
		fr.refreshData();
		TriggerDetailFragment fdetail = (TriggerDetailFragment) getFragmentManager().findFragmentByTag(TAG_FRAG_DETAIL); 
		getFragmentManager().beginTransaction().remove(fdetail).commit();
	}
    
   
    
    
}
