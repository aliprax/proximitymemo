/**
 * 
 */
package com.example.proximitymemo.view;

import java.util.List;

import com.example.proximitymemo.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

/**
 * @author Jacopo
 *
 */
public class TagsPicker extends RelativeLayout {

	private TagsViewer tv;
	private EditText et;
	private Button btn;
	
	/**
	 * @param context
	 */
	public TagsPicker(Context context) {
		super(context);
		init();
	}

	/**
	 * @param context
	 * @param attrs
	 */
	public TagsPicker(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public TagsPicker(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	public void init(){
		inflate(getContext(), R.layout.view_tags_picker, this);
		tv= ((TagsViewer)findViewById(R.id.tags_viewer));
		btn = (Button) findViewById(R.id.add_tag_button);
		et = (EditText) findViewById(R.id.edit_tag);
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String text = et.getText().toString();
				if (text!=null){
					text=text.trim();
					if (!text.equals("")){
						tv.addTag(text);
						et.setText("");
					}
				}
				
			}
		});
	} 
	
	
	public void setInitialTags(List<String> list){
		tv.setTags(list);
	}

	public List<String> getTags() {
		
		return tv.getTags();
	}

}
