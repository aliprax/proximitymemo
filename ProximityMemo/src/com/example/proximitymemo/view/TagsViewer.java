/**
 * 
 */
package com.example.proximitymemo.view;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.example.proximitymemo.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * @author Jacopo
 *
 */
public class TagsViewer extends FlowLayout {
	boolean editable;
	
	private OnClickListener removeListener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if (v instanceof TagBubble){
				tags.remove(((TagBubble) v).getText().toString());
				removeView(v);
			}
			
		}
	};
	

	/**
	 * Used by xml inflater
	 * @param context
	 * @param attrs
	 */
	public TagsViewer(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	/**
	 * Used manually by code
	 * @param context
	 * @param editable
	 */
	public TagsViewer(Context context, boolean editable) {
		super(context);
		init(context,editable);
	}

	private void init(Context context, boolean editable) {
		inflate(context,R.layout.view_tags_viewer,null);
		this.editable=editable;
	}
	
	private void init(Context context){
		init(context,true);
	}
	
	public void addTag(String tag){
		TagBubble tb= new TagBubble(getContext());
		tb.setText(tag);
		tb.setEditable(editable);
		tb.setOnRemoveTagListener(removeListener);
		tags.add(tag.toString());
		addView(tb);
	}
	
	private  List<String> tags=new LinkedList<String>();
	
	public List<String> getTags() {
		return new ArrayList<String>(tags);
	}
	
	public void setTags(List<String> tags) {
		this.tags.clear();
		removeAllViews();
		for(String tag:tags){
			addTag(tag);
		}
	}

	
	

}
