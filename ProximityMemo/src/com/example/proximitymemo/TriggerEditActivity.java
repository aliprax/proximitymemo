package com.example.proximitymemo;

import java.util.List;

import it.polimi.dei.spf.framework.local.Query;
import it.polimi.dei.spf.framework.local.SPFAction;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.framework.local.SPFTrigger;
import it.polimi.dei.spf.framework.local.SPFTrigger.IllegalTriggerException;
import it.polimi.dei.spf.lib.async.notification.SPFNotification;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class TriggerEditActivity extends Activity implements TriggerEditFragment.Callbacks{
	
	private static final String EDIT_FRAGMENT_TAG = "edit_fragment";
	SPFTrigger trigger;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trigger_edit);
		trigger = getIntent().getParcelableExtra("SPFTRIGGER");
		// savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
		if (savedInstanceState==null){
			Bundle args= new Bundle();
			TriggerEditFragment tef=TriggerEditFragment.newInstance(args);
			getFragmentManager().beginTransaction()
					.add(R.id.trigger_edit_container, tef, EDIT_FRAGMENT_TAG).commit();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.activity_trigger_edit, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_save:
			saveTheTrigger();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}

	private void saveTheTrigger() {
		TriggerEditFragment fr = (TriggerEditFragment)getFragmentManager().findFragmentByTag(EDIT_FRAGMENT_TAG);
		List<String> tags = fr.getTags();
		String triggerName = fr.getTriggerName();
		long sleepTime = fr.getSleepTime();
		boolean isOneShot = fr.getIsOneShot();
		SPFAction spfAction = fr.getSPFAction();
		Query query=null;
		
		try{
			Query.Builder builder = new Query.Builder();
			query = builder.setTagList(tags).build();
		}catch(Exception e){
			Toast.makeText(this, "illegal query", Toast.LENGTH_LONG).show();
			return;
		}
		
		try{
			createTrigger(triggerName, sleepTime, isOneShot, query, spfAction);
		}catch(IllegalTriggerException e){
			Toast.makeText(this, "trigger not valid", Toast.LENGTH_LONG).show();
			return;
		}
		
		sendSaveRequestToSPF();
		
	}

	private void sendSaveRequestToSPF() {
		SPFNotification.load(this, new SPFNotification.Callback() {
			
			@Override
			public void onServiceReady(SPFNotification notificationSvc) {
				if(notificationSvc.saveTrigger(trigger)){
					TriggerEditActivity.this.finish();
				}else{
					Toast.makeText(TriggerEditActivity.this, "Error on saving", Toast.LENGTH_LONG).show();
				}
				notificationSvc.disconnect();
			}
			
			@Override
			public void onError(SPFError errorMsg) {
				Toast.makeText(TriggerEditActivity.this, "SPF error:" + errorMsg, Toast.LENGTH_LONG).show();
				
			}
			
			@Override
			public void onDisconnected() {
				Toast.makeText(TriggerEditActivity.this, "SPF disconnected", Toast.LENGTH_LONG).show();
				
			}
		});
	}

	private void createTrigger(String triggerName, long sleepTime,
			boolean isOneShot, Query query, SPFAction spfAction)
			throws IllegalTriggerException {
		if (trigger==null){
		
			trigger=new SPFTrigger(triggerName, query, spfAction, isOneShot, sleepTime);
		}else{
			trigger.beginEdit();
			trigger.setName(triggerName);
			trigger.setQuery(query);
			trigger.setAction(spfAction);
			trigger.setSleepPeriod(sleepTime);
			trigger.setOneShot(isOneShot);
			trigger.endEdit();
		}
	}

	

	@Override
	public SPFTrigger getTrigger() {
		
		return trigger;
	}

	public static void start(final Context context,
			final long triggerId) {
		SPFNotification.load(context, new SPFNotification.Callback() {
			
			@Override
			public void onServiceReady(SPFNotification notificationSvc) {
				SPFTrigger trigger=notificationSvc.getTrigger(triggerId);
				if(trigger!=null){
					Intent intent =new Intent(context,TriggerEditActivity.class);
					intent.putExtra("SPFTRIGGER", trigger);
					context.startActivity(intent);
				}
			}
			
			@Override
			public void onError(SPFError errorMsg) {
			
				
			}
			
			@Override
			public void onDisconnected() {
				
			}
		});
		
	}
	
}
