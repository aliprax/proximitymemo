package com.example.proximitymemo;

import it.polimi.dei.spf.framework.local.SPFTrigger;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TriggerAdapter extends ArrayAdapter<SPFTrigger> {
	private Context context;
	public TriggerAdapter(Context context) {
		super(context, android.R.id.text1);
		this.context=context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if(view==null){
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
		}
		TextView textView = (TextView) view.findViewById(android.R.id.text1);
		SPFTrigger trg = getItem(position);
		textView.setText(trg.getName());
		return view;
	}
	
	

}
