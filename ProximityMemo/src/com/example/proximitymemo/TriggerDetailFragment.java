package com.example.proximitymemo;

import com.example.proximitymemo.view.TagsViewer;

import it.polimi.dei.spf.framework.local.SPFActionIntent;
import it.polimi.dei.spf.framework.local.SPFActionSendNotification;
import it.polimi.dei.spf.framework.local.SPFError;
import it.polimi.dei.spf.framework.local.SPFTrigger;
import it.polimi.dei.spf.lib.async.notification.SPFNotification;
import android.os.Bundle;
import android.app.Activity;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A fragment representing a single Trigger detail screen. This fragment is
 * either contained in a {@link TriggerListActivity} in two-pane mode (on
 * tablets) or a {@link TriggerDetailActivity} on handsets.
 */
public class TriggerDetailFragment extends Fragment {
	/*
	 * see strings.xml : @array/trigger_action_entries order
	 */
	private static final int SPF_ACTION_INTENT_ID = 0;
	private static final int SPF_ACTION_MESSAGE_ID = 1;
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_TRIGGER_ID = "trigger_id";

	/**
	 * The content this fragment is presenting.
	 */
	private SPFTrigger mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public TriggerDetailFragment() {
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (!(activity instanceof Callbacks)) {
			throw new IllegalStateException(
					"TriggerDetailfragment needs an Activity that implements its Callbacks");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	private void loadTriggerFromSPF() {
		final long triggerId = getArguments().getLong(ARG_TRIGGER_ID);
		SPFNotification.load(getActivity(), new SPFNotification.Callback() {

			@Override
			public void onServiceReady(SPFNotification notificationSvc) {

				SPFTrigger trg = notificationSvc.getTrigger(triggerId);
				TriggerDetailFragment.this.onContentReady(trg);
				notificationSvc.disconnect();
			}

			@Override
			public void onError(SPFError errorMsg) {
				Toast.makeText(getActivity(), "SPF error: " + errorMsg,
						Toast.LENGTH_LONG).show();

			}

			@Override
			public void onDisconnected() {
				Toast.makeText(getActivity(), "SPF disconnected",
						Toast.LENGTH_LONG).show();

			}
		});
	}

	// used as observer on mItem
	private Runnable updateViewProcedure = null;

	protected void onContentReady(SPFTrigger trg) {
		mItem = trg;
		if (updateViewProcedure != null && mItem != null) {
			updateViewProcedure.run();
			updateViewProcedure = null;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(
				R.layout.fragment_trigger_detail, container, false);
		View v = rootView.findViewById(R.id.trigger_detail_container_layout);
		v.setVisibility(View.INVISIBLE);
		return rootView;
	}

	private void refreshView(final View rootView) {
		if (mItem != null) {
			loadContentIntoView(rootView);
		} else {
			// register an update procedure to be called when the item is loaded
			updateViewProcedure = new Runnable() {

				@Override
				public void run() {
					loadContentIntoView(rootView);
				}
			};
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		mItem = null;
		loadTriggerFromSPF();
		refreshView(getView());
	}

	private void loadContentIntoView(View rootView) {
		// assume that mItem is not null
		((TextView) rootView.findViewById(R.id.trigger_name_text))
				.setText(mItem.getName());
		TagsViewer tv = new TagsViewer(getActivity(), false);
		tv.setTags(mItem.getQuery().getTags());
		final FrameLayout frameLayout = (FrameLayout) rootView
				.findViewById(R.id.tags_viewer_placeholder);
		frameLayout.addView(tv);
		frameLayout.invalidate();
		((CheckBox) rootView.findViewById(R.id.oneshot_checkbox))
				.setChecked(mItem.isOneShot());
		((TextView) rootView.findViewById(R.id.sleep_time_text)).setText(Long
				.toString(mItem.getSleepPeriod() / 1000) + "sec");
		if (mItem.isOneShot()) {
			rootView.findViewById(R.id.detail_section_sleep_time)
					.setVisibility(View.GONE);
		}
		
		// action section
		Spinner spinner = (Spinner) rootView.findViewById(R.id.trigger_action_spinner);
		int actionPos = mItem.getAction() instanceof SPFActionIntent? SPF_ACTION_INTENT_ID : SPF_ACTION_MESSAGE_ID;
		spinner.setSelection(actionPos);
		spinner.setEnabled(false);
		
		if (actionPos==SPF_ACTION_INTENT_ID){
			rootView.findViewById(R.id.action_message_content).setVisibility(View.GONE);
			rootView.findViewById(R.id.action_message_title).setVisibility(View.GONE);
			
		}else{	
			SPFActionSendNotification actionSN = (SPFActionSendNotification) mItem.getAction();
			String message = actionSN.getMessage();
			TextView textMessageContent = (TextView) rootView.findViewById(R.id.action_message_content);
			textMessageContent.setText(message);
			textMessageContent.setVisibility(View.VISIBLE);
			String title = actionSN.getTitle();
			TextView textMessageTitle = (TextView) rootView.findViewById(R.id.action_message_title);
			textMessageTitle.setText(title);
			textMessageTitle.setVisibility(View.VISIBLE);
		}
		rootView.findViewById(R.id.trigger_detail_container_layout)
		.setVisibility(View.VISIBLE);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.fragment_trigger_detail, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_delete:
			deleteTrigger();
			return true;
		case R.id.action_edit:
			if (mItem == null) {
				return true;
			}
			final long triggerId = mItem.getId();
			TriggerEditActivity.start(getActivity(), triggerId);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void deleteTrigger() {
		SPFNotification.load(getActivity(), deleteCallback);
	}

	SPFNotification.Callback deleteCallback = new SPFNotification.Callback() {

		@Override
		public void onServiceReady(SPFNotification notificationSvc) {
			if (mItem == null) {
				notificationSvc.disconnect();
				((Callbacks) getActivity()).onItemDeleted();
			}
			final long triggerId = mItem.getId();
			if (!notificationSvc.deleteTrigger(triggerId)) {
				Toast.makeText(getActivity(), "error on deleting",
						Toast.LENGTH_LONG).show();
			}
			notificationSvc.disconnect();
			((Callbacks) getActivity()).onItemDeleted();
		}

		@Override
		public void onError(SPFError errorMsg) {
			Toast.makeText(getActivity(), "SPF error: " + errorMsg,
					Toast.LENGTH_LONG).show();
		}

		@Override
		public void onDisconnected() {
			Toast.makeText(getActivity(), "SPF disconnected", Toast.LENGTH_LONG)
					.show();
		}
	};

	interface Callbacks {
		void onItemDeleted();
	}
}
